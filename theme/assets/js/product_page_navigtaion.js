var $j = jQuery.noConflict();

$j(document).ready(function() {

	/*================================= 
	PRODUCT PAGE NAVIGATIONS
	Dot nav to smooth scroll to panels on the product pages.
	=================================*/

	/*=== Fade in the navigation dots only when a user arrives at the first panel. ===*/

	if($j('.trago_product_panel').length) {
		$j(window).scroll(function(){
			// Get the scroll distance from the top of the window.
			var scrollPosition = $j(this).scrollTop();
			// Get the position of the first panel from the bottom of the navigation. Which is the window minus the height of the nav.
			var firstPanelPosition = $j('.trago_product_panel#panel1').offset().top - 120;
			// If the scroll position is greater than the first panels position then show the navigation dots.
			if(scrollPosition >= firstPanelPosition) {
				$j('.page_navigation_dots').addClass('visible');
			}
			// Else hide the navigation dots
			else {
				$j('.page_navigation_dots').removeClass('visible');
			}
		});
	}

	/*=== Click action on dots to scroll to respective panel in the page. ===*/

	$j('.page_navigation_dots li').click(function(){
		// Capture the target panel we'd like to scroll to.
		var targetPanel = $j(this).attr('data-target');
		// Find the distance of that panel from the top of the window.
		var targetPanelDistance = $j('.trago_product_panel#' + targetPanel).offset().top;
		// Animate the body of the page to scroll to the target panel.
		$j('body, html').animate({
	        'scrollTop': targetPanelDistance - 100
	    }, 900, 'swing');
	});

	/*=== Scroll action on page to indicate which panel is currently within the viewport. ===*/

	if($j('.trago_product_panel').length) {
		$j(window).scroll(function(){
			var scrollPosition = $j(this).scrollTop();
			$j('.trago_product_panel').each(function(){
				var panelOffset = $j(this).offset().top;
				var panelHeight = $j(this).height();
				var endOfPanelOffset = panelOffset + panelHeight;
				var dotNavigationTarget = $j(this).attr('id');
				// If the scroll position is greater than the panel distance from the top of the window and if less than the distance of the outer panel.
				if((scrollPosition >= panelOffset - 120) && (scrollPosition <= endOfPanelOffset)) {
					$j('.page_navigation_dots li').removeClass('active');
					$j('li[data-target=' + dotNavigationTarget + ']').addClass('active');
				}
			});
		});
	};

});