var $j = jQuery.noConflict();

$j(document).ready(function() {

	/*======================== 
	ADD TO CART AJAX
	========================*/
	    
	$j('#product_form').submit(function(e) {
	    
		e.preventDefault();

		var that = $j(this);

		/*=== Get the ID of the selected variant ===*/

		var variant_selected_id = $j(this).find('input#add_to_cart').attr('data-variant-id');

		/*=== Get the quantity ===*/

		var product_quantity = $j(this).find('input[type="number"]').val();

		/*=== Send the product to the cart using ajax ===*/

		$j.post('/cart/add.js', {quantity: product_quantity, id: variant_selected_id}, function(data) {

		/*=== Display/Hide a message when succesful ===*/

			$j(that).find('.ajax_message').text(product_quantity + " " + data.product_title + " " + data.variant_title + " was succesfully added to your cart.").fadeIn(1000).delay(3000).fadeOut(1000, function() {

				$j(this).empty();

			});

			/*=== Cart Count Modifications ===*/

			$j.getJSON('/cart.js', function(cartdata) {

				$j('#cart_count').html('[' + cartdata.item_count + ']');

			});

		}, 'json');

	});
});