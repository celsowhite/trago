var $j = jQuery.noConflict();

$j(document).ready(function() {

	/*================================= 
	INITIALIZE PRODUCT SLIDER
	=================================*/

	$j('.product_slider').flexslider({
		animation: "fade",
		controlNav: true,
		directionNav: false,
		animationLoop: false,
		slideshow: false,
		manualControls: '.product_thumbnails li'
	});

	/*================================= 
	Utility to switch product images based on color
	=================================*/

	var productImageSwitcher = function(variantColor) {

		// Fade out all visible thumbnails. Then fade in the new color.

		$j('ul.product_thumbnails li.visible').fadeOut(500, function(){
			$j(this).removeClass('visible');
			$j('ul.product_thumbnails li[data-color="' + variantColor + '"]').fadeIn(500).addClass('visible');
		});

		// Find the position of the first thumbnail to animate the main slider to the respective image.

		var firstThumbnail = $j('ul.product_thumbnails li[data-color="' + variantColor + '"]').first();

		var firstThumbnailPosition = firstThumbnail.index();

		// Animate the slider to the first image of a specific color

		productSliderFlexData.flexAnimate(firstThumbnailPosition);

	}

	/*================================= 
	On page load set the right product images and form information.
	=================================*/

	// Save the slider DOM element

	var productSlider = $j('.product_slider');

	hasProductSlider = productSlider.length;

	var productSliderFlexData = productSlider.data('flexslider');

	// Parse the browser URL for the variant ID

	var query = window.location.search.substring(1);

	var querySplit = query.split('=');

	var urlVariantId = querySplit[1];

	// If the url has a query attached

	if(urlVariantId && hasProductSlider) {

		// Find the radio button that corresponds to the ID

		var selectedVariant = $j('.trago_color_variants input[type="radio"][id="' + urlVariantId + '"]');

		var selectedVariantColor = selectedVariant.attr('data-color');

		// Set the radio button for the specific variant ID to checked

		selectedVariant.attr('checked', 'checked');

		// Set the add to cart variant ID to the correct variant

		$j('#add_to_cart').attr('data-variant-id', urlVariantId);

		// Show the images for the selected variant

		productImageSwitcher(selectedVariantColor);

	}

	// If no query attached to URL then show the first variant

	else if(hasProductSlider) {

		// Capture the first radio button color

		var firstRadioButton = $j('.trago_color_variants input[type="radio"]').first();

		var firstRadioColor = firstRadioButton.attr('data-color');

		// Set the first variant radio button as checked

		$j('.trago_color_variants input[type="radio"]:first-child').attr('checked', 'checked');

		// Show the images related to the first radio button

		productImageSwitcher(firstRadioColor);

	}

	/*================================= 
	Select a specific color variant then adjust the images
	=================================*/

	$j('.trago_color_variants input[type="radio"]').click(function(){

		var variantColor = $j(this).attr('data-color');

		productImageSwitcher(variantColor);

	});

});