var $j = jQuery.noConflict();

$j(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

	/*================================= 
	DOWN ARROW SMOOTH SCROLL
	=================================*/

	var downArrow = $j('.down_arrow i');
	var pageContent = $j('main.page_content');

	if(pageContent.length) {
		var pageContentDistance = pageContent.offset().top;

		downArrow.click(function(){
			$j('body, html').animate({
		        'scrollTop': pageContentDistance - 80
		    }, 900, 'swing');
		});
	}

	/*================================= 
	MOBILE MENU
	=================================*/

	$j('.mobile_menu_icon').click(function(){
		$j('.mobile_menu').toggleClass('open');
	});

	/*================================= 
	LOGIN
	=================================*/

	// First check if the page has the recover hash

	if(window.location.pathname === "/account/login" && window.location.hash === "#recover"){
		// Change the login title
		$j('#login_title').text('Recover Password');
		// Make sure recover password form is showing
		$j('.customer_login_form').fadeOut(function(){
			$j('.recover_password_form').fadeIn();
		});
	};

	// Forgot password form toggle

	$j('#forgot_password').click(function(e){
		e.preventDefault();
		// Change the login title
		$j('#login_title').text('Recover Password');
		// Switch out the form
		$j('.customer_login_form').fadeOut(function(){
			$j('.recover_password_form').fadeIn();
		});
	})

	// Cancel forget password

	$j('#cancel_forgot_password').click(function(e){
		e.preventDefault();
		// Change the login title
		$j('#login_title').text('Login');
		// Switch out the form
		$j('.recover_password_form').fadeOut(function(){
			$j('.customer_login_form').fadeIn();
		})
	})

	/*================================= 
	PRODUCT SUBNAV REVEAL
	=================================*/

	// If the page is not a product page then create the hover effect on the product navigation.

	if($j('.trago_product_panel').length === 0) {
		$j('.product_nav_item').hover(function(){
			$j('.product_navigation').toggleClass('visible');
		});

		$j('.product_navigation').hover(function(){
			$j(this).toggleClass('visible');
		});
	}

	/*================================= 
	PRESS TOGGLE
	=================================*/

	// On click of a press logo, toggle its quote

	$j('.press_images li').click(function(){
		var pressName = $j(this).attr('data-name');
		console.log(pressName);
		// Change the active image

		$j('.press_images li.active').removeClass('active');

		$j(this).addClass('active');

		// Toggle the quote

		$j('.press_quotes p.active').fadeOut(function(){
			$j(this).removeClass('active');
			$j('.press_quotes p.' + pressName).fadeIn().addClass('active');
		})
	});
	
	/*================================= 
	Custom quantity increase/decrease buttons
	=================================*/

	$j('.quantity_up').click(function(){
		// Find the quantity input. Using closets because on the cart page there is more than one.
		var quantityInput = $j(this).closest('.quantity_input_container').find('input[type=number]');
		// Get the value of the input.
		var quantityValue = parseInt(quantityInput.val(), 10);
		// Increase the value by 1.
		quantityValue += 1;
		// Set the new value on the input
		quantityInput.val(quantityValue);
		// Invoke the onChange function on the input so it runs the proper functions after it has been updated.
	  	quantityInput.change();
	});

	$j('.quantity_down').click(function(){
	  	var quantityInput = $j(this).closest('.quantity_input_container').find('input[type=number]');
		var quantityValue = parseInt(quantityInput.val(), 10);
		quantityValue -= 1;
		if(quantityValue > 0) {
			quantityInput.val(quantityValue);
	  		quantityInput.change();
		}
	});

	/*================================= 
	LOADING
	=================================*/

	setTimeout(function(){
		$j('body').addClass('loaded');
		$j('.blue_loading_area').addClass('loaded');
	}, 1000);

	/*================================= 
	MAILCHIMP AJAX
	=================================*/

	// Set up form variables

	var mailchimpForm = $j('.mc-embedded-subscribe-form');

	// On submit of the form send an ajax request to mailchimp for data.

	mailchimpForm.submit(function(e){

		// Set variables for this specific form
		var that = $j(this);
		var mailchimpSubmit = $j(this).find('input[type=submit]');
		var mailchimpEmail = $j(this).find('input[type=email]');
		var errorResponse = $j(this).closest('.mc_embed_signup').find('.mce-error-response');
		var successResponse = $j(this).closest('.mc_embed_signup').find('.mce-success-response');

		// Make sure the form doesn't link anywhere on submit.
		e.preventDefault();
		// JQuery AJAX request http://api.jquery.com/jquery.ajax/
		$j.ajax({
		  method: 'GET',
		  url: that.attr('action'),
		  data: that.serialize(),
		  dataType: 'jsonp',
		  success: function(data) {
		  	// If there was an error then show the error message.
		  	if (data.result === 'error') {
		  		// Hide the first few characters int the error message string which display the error code and hyphen.
		  		var messageWithoutCode = data.msg.slice(3);
		  		errorResponse.text(messageWithoutCode).show(300).delay(2000).hide(300);
		  	}
		  	// If success then show message
		  	else {
		  		successResponse.text('Success!').show(300).delay(2000).hide(300);
		  	}
		  }
		});
	});

	/*================================= 
	FITVIDS
	=================================*/

	/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

	$j('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

	/*=== Target div for fitVids ===*/

	$j(".video_embed").fitVids();

	/*================================= 
	FLEXSLIDER
	=================================*/

	// General

    $j('.trago_slider').flexslider({
    	animation: "fade",
    	controlNav: true,
    	directionNav: false,
    	slideshowSpeed: 1000
    });

    /*================================= 
	PRODUCT TABS
	=================================*/

	// Switch out the product tab content

	$j('.product_tabs li').click(function(){

		var tabName = $j(this).attr('data-name');

		// Change the active tab link

		$j('.product_tabs li.active').removeClass('active');

		$j(this).addClass('active');

		// Toggle the tab content

		$j('.product_tab_content.active').fadeOut(function(){
			$j(this).removeClass('active');
			$j('.product_tab_content.' + tabName).fadeIn().addClass('active');
		})
	});

});