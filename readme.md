# About #

Shopify starter template based on [Shopify Skeleton](https://github.com/Shopify/skeleton-theme). Uses [Quickshot](https://quickshot.readme.io/v2.1/docs) to sync the files between local and live dev site.

# Installation Instructions #

1. Create a local folder named 'your-shop-name'.
2. Clone this repo into the folder. The folder named 'theme' should remain as 'theme'. This is for the quickshot commands.
3. Zip the 'theme' folder, rename it to your new theme name, upload it to the store and publish it.
4. Create a private app in your store. Go to your-store.myshopify.com/admin/apps/private. Ensure that your private shopify app has read/write access to the theme files.
5. Delete the zipped 'theme-name' folder from your local machine.
6. Run 'qs configure' command. Follow the installation guide. Also ensure you set compile scss to false. This repo uses gulp to autoprefix, compile scss and minify assets.
7. Run the 'qs theme watch' command. Your files are now being watched and will auto sync with your shopify store.
8. Run 'gulp' to watch the scss and js files.
9. You are ready to go.
